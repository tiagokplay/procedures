CREATE proc rede_fechamento_novaCiclo37_asos --35              
 @ciclo int = 0                        
as                        
set nocount on                        
      -- exec rede_fechamento_asos 0               
              
--acerto da classificacao da fat{_hierarquia              
update fh set classificacao = 10 from fat_hierarquia fh               
  inner join fat_comercio fc on fh.destinatario=fc.destinatario where fc.grupo >= 4 and fh.classificacao<=5              
                    
if @ciclo = 0                        
begin                        
 select @ciclo = ciclo from rede_parametros where convert(datetime, convert(varchar(10),GETDATE(),120)) between inicio and fim                        
end                        
                        
declare @inicio datetime, @fim datetime--, @ciclo int = 36              
select @inicio = inicio, @fim = fim from rede_parametros where ciclo = @ciclo                        
--if (@ciclo = (select ciclo from rede_parametros where convert(datetime, convert(varchar(10),GETDATE(),120)) between inicio and fim ))                        
                        
--update ped_PedidoTotal set ciclo = @ciclo where convert(datetime, convert(varchar(10),DataPedido,120)) between @inicio and @fim                 
              
--atribuir ciclo atual apenas para pedidos pagos no ciclo atual              
select distinct pms.pediCodigo into #pedidos_pagos from ped_MovimentoStatus pms              
 inner join ped_PedidoTotal ppt on ppt.pediCodigo = pms.pediCodigo              
 inner join rede_parametros rp on rp.ciclo = @ciclo              
where CONVERT(date,pms.DataMovimento) between rp.inicio and rp.fim and pms.stnfCodigo in ( 2, 10, 11, 12, 97, 98, 200) and ppt.ciclo is null                
              
         
        
update ped_PedidoTotal set ciclo = @ciclo where pediCodigo in (select pediCodigo from #pedidos_pagos)                
                     
declare @i int                        
                                          
select top 1 * into #rede_apuracao from rede_apuracao                                          
truncate table #rede_apuracao                                          
                                          
update a set hieComprador = (select top 1 hierarquia from fat_hierarquia h where h.destinatario = a.destinatario) from ped_PedidoTotal a                                          
                                          
/*Criar tabela de pedidos de Kii Inicio do m�s*/              
select ppt.destinatario, ppt.pedicodigo into #kit_inicio from ped_pedidototal ppt              
 inner join ped_pedidoitem ppi on ppt.pedicodigo = ppi.pedicodigo              
 inner join sys_produto sp on ppi.prodcodigo = sp.produto              
where ciclo = @ciclo and linha in (1)               
              
delete from rede_kitInicio where ciclo = @ciclo              
insert into rede_kitInicio              
 select *, ciclo =@ciclo from #kit_inicio              
/*********************************************/              
              
--DEFINE QUAIS LIDERES PERDERAM A QUALIFICA��O              
insert into rede_lideres_hist(hie,ciclo,data,aceite,fim_vigencia)              
select rl.hie,               
    rl.ciclo,              
    rl.data,              
    rl.aceite,              
    fim_vigencia = rp1.fim              
from rede_apuracao ra1              
inner join rede_parametros rp on rp.inicio = ra1.data --CICLO ATUAL              
inner join rede_parametros rp1 on rp1.ciclo = rp.ciclo-1 --CICLO ANTERIOR              
inner join rede_lideres rl on rl.hie = ra1.hierarquia and rl.ciclo < rp.ciclo              
left join rede_lideres_hist rlh on rlh.hie = rl.hie and rlh.ciclo = rl.ciclo              
where ra1.data = @inicio              
   and ra1.qualificacao >= 20              
   and ra1.hierarquia <> 1              
   and rlh.hie is null              
   and not exists (              
  select 1 from rede_apuracao ra2               
  inner join rede_parametros rp on rp.ciclo in (@ciclo-1,@ciclo-2,@ciclo-3) and ra2.data = rp.inicio and ra1.hierarquia = ra2.hierarquia              
  where isnull(ra2.qualificacaoAtual,0) >= 20              
   )               
                 
select hie,fim_vigencia = max(fim_vigencia) into #rede_lideres_hist from rede_lideres_hist group by hie              
              
delete ra from rede_lideres ra              
inner join rede_parametros rp on rp.ciclo = ra.ciclo              
inner join #rede_lideres_hist rlh on rlh.hie = ra.hie and rp.inicio <= rlh.fim_vigencia              
              
select top 1 a.pedicodigo, a.destinatario, b.hierarquia, a.hieComprador, a.pontos_calculados,a.datafaturamento,a.datapedido, a.stnfcodigo, a.negocios_calculados, a.ciclo                        
 into #ped_pedidototal             
 from ped_PedidoTotal a inner join fat_hierarquia b on a.destinatario = b.destinatario                        
truncate table #ped_pedidototal                        
            insert into #ped_pedidototal                        
select a.pedicodigo, a.destinatario, b.hierarquia, a.hieComprador, a.pontos_calculados,a.datafaturamento,a.datapedido, a.stnfcodigo, a.negocios_calculados, a.ciclo                        
  from ped_pedidototal a inner join fat_hierarquia b on a.destinatario = b.destinatario                        
  where  ciclo = @ciclo and a.stnfCodigo in (1, 2, 10, 11, 12, 97, 98, 200)                        
                        
 declare @cicloant int, @dataant datetime                        
 select @cicloant = ciclo, @dataant = inicio from rede_parametros where ciclo = (@ciclo - 1)                        
------------------------------------------------------------------------------------------------------------------------------------------------------                        
-- Gerando hierarquias na rede_apuracao                        
 insert into #rede_apuracao(hierarquia, data, tipo_bonus, ciclo)                        
  select distinct hierarquia, @inicio, 0, @ciclo from fat_hierarquia                        
                        
 update #rede_apuracao set data = @inicio , pontos = 0                        
                        
                        
--update a set kit = vcc.produto from #rede_apuracao a inner join view_consultor vc on a.hierarquia = vc.hierarquia inner join view_Consultor_Concessao vcc on vc.destinatario = vcc.destinatario                        
----------------------------------------------------------------------------------------------------------------------------                
-- Verificando volume de grupo               
 select pt.pedicodigo, pt.destinatario, (SUM(pt.pontos_calculados)) as ProdPontos, SUM(valorTotalNf) as valores,                        
  hieComprador , datapedido= isnull(DataFaturamento,DataPedido)                        
  into #pontoss                      
  from ped_pedidototal pt              
 inner join fat_comercio fc on pt.destinatario=fc.destinatario  and fc.grupo >= 4 /*Considerar apenas os consultores, ou seja, excluir os consumidores, distribuidores e Franquia-CD*/              
  where  ciclo = @ciclo                      
   and pt.pedicodigo not in (select pedicodigo from rede_kitInicio where ciclo = @ciclo) /*Excluir kit inicio (Gilson em 30/03/2017)*/              
   and stnfCodigo in (1, 2, 10, 37, 42, 43, 97, 98, 200) --and pt.pediCodigo in (select pedido from fat_SAIDA where situacao = 0)                                          
  group by pt.pedicodigo, pt.destinatario, hieComprador, DataFaturamento, DataPedido                 
                     
-- Atribuindo pontua��o no m�s                      
update a set pontos = ISNULL((select sum(prodPontos) from #pontoss x where x.hiecomprador = a.hierarquia),0) from #rede_apuracao a                 
              
--Atribuindo maior qualifica��o              
--Obs 1.0: � considerado a qualifica��o do ciclo atual pois o VOLUME DO GRUPO depende dela              
--Obs 1.1: A rede_lideres define quando o consultor atingiu a qualifica��o(maior) atual              
--Obs 1.2: A rede_lideres_hist define quando o consultor atingiu a qualifica��o e quando a qualifica��o caiu.              
              
update b               
set qualificacao = isnull(isnull(fdo.qualificacao, ra2.qualificacao),10)                             
from #rede_apuracao b                                        
left join (              
 select fh.hierarquia,               
     qualificacao =max(condicao)              
 from rede_parametros rp               
 inner join fat_destinatario_ocorrencia fdo on fdo.ocorrencia in (331,332,333,334,335,336,337,338,339) and convert(date,fdo.data) between rp.inicio and rp.fim              
 inner join fat_ocorrencia fo on fo.ocorrencia = fdo.ocorrencia              
 inner join fat_hierarquia fh on fh.destinatario = fdo.destinatario              
 where rp.inicio = @inicio              
 group by fh.hierarquia              
) fdo on fdo.hierarquia = b.hierarquia --QUALIFICA��O MANUAL                      
left join (              
    select a.hierarquia, qualificacao = max(qualificacao) from rede_apuracao a               
    left join #rede_lideres_hist rlh on rlh.hie = a.hierarquia              
    where   a.ciclo <= @ciclo              
      and (              
       a.data > isnull(rlh.fim_vigencia,'1900-01-01')              
       )              
    group by a.hierarquia              
) ra2 on ra2.hierarquia = b.hierarquia --QUALIFICA��O POR HIST�RICO DE QUALIFICA��ES                              
    
    
 ---------------------------------------------------- LIDERES QUE DESISTIRAM DE SER LIDERES -------------------------------------------------    
    
UPDATE ra    
SET qualificacao = 10    
FROM #rede_apuracao ra    
INNER JOIN view_consultor_v2 vc on vc.hierarquia = ra.hierarquia    
INNER JOIN fat_destinatario_ocorrencia fdo on fdo.destinatario = vc.destinatario and fdo.ocorrencia in (343)     
    
---------------------------------------------------------------------------------------------------------------------------------------------    
    
--update #rede_apuracao set qualificacao = 20 where hierarquia = 1742              
                   
delete rede_apuracaoVolumeGrupo where  ciclo = @ciclo              
              
-- Atribuindo Volume de Grupo de pessoa que n�o atingiram a maior qualifica��o(Lider) m�nima.                 
insert into rede_apuracaoVolumeGrupo(ciclo,hierarquia,permissao,profundidade,pedicodigo,pontos_calculados,valorTotalNf,qualificacao)              
select ciclo = @ciclo,              
    hierarquia = ra.hierarquia,              
    permissao = fhv.permissao,              
    profundidade = fhv.profundidade,              
    pedicodigo = p.pedicodigo,              
    pontos_calculados = p.ProdPontos,              
    valorTotalNf = p.valores,              
    qualificacao = ra.qualificacao              
from #rede_apuracao ra              
inner join fat_hierarquia_view fhv on fhv.hierarquia = ra.hierarquia and fhv.profundidade = 1              
inner join #pontoss p on p.hieComprador = fhv.permissao              
where ra.qualificacao = 10                  
              
/*Criar tempor�ria dos l�deres*/              
select hierarquia, qualificacao into #lideresQualificacao from #rede_apuracao where qualificacao >= 20              
              
/*fat_hierarquia_view dos l�deres*/              
select * into #redeLiderencaQualificacao from fat_hierarquia_view where hierarquia in (select hierarquia from #lideresQualificacao)              
              
/*Deletar as permiss�es abaixo dos l�deres do liderPai*/              
select lt1.hierarquia,              
    lt2.permissao               
into #deletarRedeOutrosLideres               
from #redeLiderencaQualificacao lt1              
inner join #redeLiderencaQualificacao lt2 on lt1.permissao = lt2.hierarquia and (lt1.hierarquia <> lt2.hierarquia and lt1.permissao=lt2.hierarquia) and lt2.profundidade >=0               
order by lt1.hierarquia, lt1.permissao, lt2.profundidade              
              
        
               
--Retirando remo��o dos primeiros lideres              
select hieUpline = fhv1.hierarquia,              
    qualificacaoUpline = ra1.qualificacao,              
    profundidadeUpline = fhv1.profundidade,              
    hieDownline = fhv2.hierarquia,              
    permissaoDownline = fhv2.permissao,              
    patrocinadorDownline = fh1.nivel_superior,              
    qualificacaoDownline = ra2.qualificacao,              
    profundidadeDownline = fhv2.profundidade               
into #tempLideresProcessamento               
from fat_hierarquia_view fhv1              
inner join #rede_apuracao ra1 on ra1.hierarquia = fhv1.hierarquia and ra1.qualificacao >= 20 -- and ra1.data = '2017-06-01'               
inner join #rede_apuracao ra2 on ra2.hierarquia = fhv1.permissao and ra2.qualificacao >= 20 --and ra2.data = '2017-06-01'              
inner join fat_hierarquia_view fhv2 on fhv2.hierarquia = fhv1.permissao and fhv2.profundidade = 0              
inner join fat_hierarquia fh1 on fh1.hierarquia = fhv2.permissao              
where fhv1.profundidade > 0              
order by fhv1.hierarquia,fhv1.profundidade,fhv2.hierarquia,fhv2.profundidade              
              
select * into #lideresUplines from #tempLideresProcessamento a              
cross apply dbo.fn_raizArvore(a.permissaoDownline,0) as b              
order by a.hieUpline,a.profundidadeUpline,hieDownline,b.profundidade              
              
              
declare @maiorProfundidade int = isnull((select maiorProfundidade = max(profundidade) from fat_hierarquia_view fhv),0),              
     @profundidadesProcessadas int = 0              
              
create table #primeiroLiderRede(              
hierarquia int,              
permissao int           
)              
              
while @maiorProfundidade  >= @profundidadesProcessadas              
begin              
 select @profundidadesProcessadas = @profundidadesProcessadas + 1              
              
 insert into #primeiroLiderRede              
 select lu1.hieUpline,               
     lu1.hieDownline              
 from #lideresUplines lu1              
 where lu1.profundidadeUpline = @profundidadesProcessadas              
    and lu1.profundidade = 0              
    and not exists (              
    select 1 from #lideresUplines lu2               
    inner join #primeiroLiderRede lpr on lpr.hierarquia = lu1.hieUpline and lpr.permissao = lu2.hierarquia              
    where lu2.hieUpline = lu1.hieUpline               
      and lu2.hieDownline = lu1.hieDownline               
      and lu2.profundidade > 0               
    )              
end              
              
/* DEBUG              
              
 select a.hierarquia,b.permissao,a.profundidade from fat_hierarquia_view a              
 inner join #primeiroLiderRede b on b.hierarquia = a.hierarquia and b.permissao = a.permissao              
              
*/              
              
              
delete dr from #deletarRedeOutrosLideres dr              
inner join #primeiroLiderRede pl on pl.hierarquia = dr.hierarquia and  pl.permissao = dr.permissao              
              
delete t from #redeLiderencaQualificacao t               
inner join #deletarRedeOutrosLideres d on t.hierarquia = d.hierarquia and t.permissao=d.permissao              
              
              
insert into rede_apuracaoVolumeGrupo(ciclo,hierarquia,permissao,profundidade,pedicodigo,pontos_calculados,valorTotalNf,qualificacao)              
select  ciclo = @ciclo,              
  hierarquia = ra.hierarquia,              
  permissao = rlq.permissao,              
  profundidade = rlq.profundidade,              
  pedicodigo = p.pedicodigo,              
  pontos_calculados = p.ProdPontos,              
  valorTotalNf = p.valores,              
  qualificacao = ra.qualificacao              
from #rede_apuracao ra              
inner join #redeLiderencaQualificacao rlq on rlq.hierarquia = ra.hierarquia              
inner join #pontoss p on p.hieComprador = rlq.permissao              
where ra.qualificacao > 10 and rlq.profundidade > 0              
              
 update a set vol_grupo = ISNULL((select sum(pontos_calculados) from rede_apuracaoVolumeGrupo rav where a.hierarquia = rav.hierarquia and rav.ciclo = @ciclo ),0)                        
 from #rede_apuracao a                
               
---- Atribuindo Valores do Grupo                        
-- update a set faturamentoGrupo = ISNULL((select sum(valores) from #pontoss                       
-- inner join fat_hierarquia_view fhv on #pontoss.hieComprador = fhv.permissao and fhv.hierarquia = a.hierarquia and fhv.profundidade > 0                      
-- /*inner join #rede_apuracao ra1 on fhv.hierarquia = ra1.hierarquia and ra1.master1 = a.hierarquia*/),0)              
-- from #rede_apuracao a                        
                        
----------------------------------------------------- B�nus Internet ---------------------------------------------------                
select prodCodigo, ValorTotalItem as totProd, rota, Qtd                      
 into #BonusInternet                      
 from ped_PedidoTotal ppt inner join ped_pedidoitem ppi on ppt.pediCodigo = ppi.pediCodigo                      
 inner join sys_produto sp on sp.produto = ppi.prodCodigo and sp.linha not in (1, 25, 11, 26)  /*1 kit inicio, 2 material de apoio, 25 upgrade e 26 ativa��o mensal*/              
 where rota is not null and ppt.ciclo = @ciclo and ppt.stnfCodigo in (1, 2, 10, 11, 42, 43, 97, 98, 200)                      
              
update a set totProd = totProd - (b.preco_base * qtd) from #BonusInternet a inner join fat_FAIXA_PRECO b on a.prodCodigo = b.produto where b.faixa = 2                      
alter table #BonusInternet add total numeric(18,2)              
              
update a set total = totProd * .15  from #BonusInternet a                      
              
select rota, SUM(total) as total into #BInternet from #BonusInternet group by rota                      
              
update ra set bonusInternet = bi.total from #BInternet bi, #rede_apuracao ra where bi.rota = ra.hierarquia                      
----------------------------------------------------------------------------------------------------------------                      
----------------------------------------------------------------- Qualifica��o -------------------------------------------------------------------------------                                                       
delete from #rede_apuracao where hierarquia in (select hierarquia from view_consultor where grupo in (1,3))                     
                  
--Consultores                  
update ap set qualificacaoAtual = 10 from #rede_apuracao ap inner join fat_hierarquia fh on fh.hierarquia = ap.hierarquia                   
 where fh.classificacao = 10 or fh.classificacao = 20               
              
--altera��o dia 06/06/2017 setar lideres manualmente              
insert into rede_lideres              
 select ap.hierarquia, @ciclo, GETDATE(), 0 from #rede_apuracao ap inner join fat_hierarquia fh on fh.hierarquia = ap.hierarquia                   
 where ap.vol_grupo >= 4000 and ap.hierarquia not in (select hie from rede_lideres)              
              
--Primeiros Lideres                  
update ap set qualificacaoAtual = 20 from #rede_apuracao ap inner join fat_hierarquia fh on fh.hierarquia = ap.hierarquia                   
 where ap.vol_grupo >= 4000 and ap.hierarquia in (select hie from rede_lideres where aceite = 1)             
 and ( fh.destinatario in (select destinatario from fat_destinatario_ocorrencia where ocorrencia = 342) or @ciclo < 42 )            
              
--update #rede_apuracao set qualificacaoAtual = 20 where hierarquia = 1742              
              
-- Rede do l�der              
update f set lider1 = isnull((select top 1 v.hierarquia from fat_hierarquia_view v                      
     inner join #rede_apuracao f1 on f1.hierarquia = v.hierarquia                        
                    where f.hierarquia = v.permissao and v.profundidade >= 0 and (f1.qualificacao >= 20 or f1.qualificacaoAtual = 20) and f1.hierarquia in (select hie from rede_lideres where aceite = 1) order by profundidade),0)                 
  from #rede_apuracao f                 
  where f.data = @inicio                   
              
              
/*totNovosLideres = agora � o total de l�deres diretos e n�o NOVOS l�deres diretos*/                
select fhv.hierarquia, fhv.permissao into #diretosQ from fat_hierarquia_view fhv inner join #rede_apuracao ra on ra.hierarquia = fhv.hierarquia where profundidade = 1 order by hierarquia, permissao             
              
alter table #diretosQ add totNovosLideres int, qualificacao int, maxQualificacao int, ganho numeric(18,2)                   
                
update a set totNovosLideres = isnull((select isnull(count(permissao),0) from fat_hierarquia_view fhv              
   inner join #rede_apuracao ra on fhv.permissao= ra.hierarquia and qualificacaoAtual >=20 and profundidade = 1 and fhv.hierarquia=a.hierarquia group by fhv.hierarquia),0)              
 from #rede_apuracao a              
              
              
-- Atribuindo Valores do Grupo (faturamento )                      
--update a set faturamentoGrupo = ISNULL((select sum(valores) from #pontoss                       
-- inner join fat_hierarquia_view fhv on #pontoss.hieComprador = fhv.permissao and fhv.hierarquia = a.hierarquia and fhv.profundidade > 0                      
-- inner join #rede_apuracao ra1 on fhv.hierarquia = ra1.hierarquia and ra1.lider1 = a.hierarquia and ),0)              
-- from #rede_apuracao a                
              
              
--update a set faturamentoGrupo = ISNULL((select sum(valores) from #pontoss                       
-- inner join fat_hierarquia_view fhv on #pontoss.hieComprador = fhv.permissao-- and fhv.profundidade = 1                      
-- inner join #diretosQ d on d.hierarquia=a.hierarquia              
-- inner join #rede_apuracao ra1 on fhv.hierarquia = ra1.hierarquia and ra1.lider1 = a.hierarquia and ),0)              
-- from #rede_apuracao a                
              
              
              
--update a set faturamentoGrupo = ISNULL((select sum(isnull(p.valores,0)) from #diretosQ d               
-- inner join #rede_apuracao ra2 on d.permissao=ra2.lider1              
-- inner join #pontoss p on p.hiecomprador=ra2.hierarquia and d.hierarquia=a.hierarquia              
-- inner join fat_hierarquia_view fhv on fhv.permissao=ra2.hierarquia),0)              
-- from #rede_apuracao a              
              
               
--update a set faturamentoGrupo = ISNULL((select sum(isnull(p.valores,0)) from #diretosQ d               
-- inner join #rede_apuracao ra2 on d.permissao=ra2.lider1              
-- inner join fat_hierarquia_view fhv on fhv.hierarquia=d.permissao              
-- inner join #pontoss p on p.hiecomprador=ra2.hierarquia and d.hierarquia=a.hierarquia),0)              
-- from #rede_apuracao a              
              
              
              
update a set  faturamentoGrupo = ISNULL((select distinct sum(isnull(p.valores,0)) from #pontoss p              
 inner join #rede_apuracao ra1 on p.hiecomprador=ra1.hierarquia   --ra1.lider1              
 inner join #diretosQ q on q.permissao=ra1.lider1              
 inner join #rede_apuracao ra2 on q.permissao=ra2.hierarquia and (ra2.qualificacao>=20 or ra2.qualificacaoAtual>=20) and ra2.hierarquia=a.hierarquia),0) from #rede_apuracao a --where a.hierarquia = ra2.hierarquia              
              
/*Criar tempor�ria dos l�deres*/              
select hierarquia, qualificacaoAtual into #lideres from #rede_apuracao where qualificacaoAtual >= 20              
              
/*fat_hierarquia_view dos l�deres*/              
select * into #t from fat_hierarquia_view where hierarquia in (select hierarquia from #lideres)              
select * into #t2 from #t              
              
/*Deletar as permiss�es abaixo dos l�deres do liderPai*/     
select lt1.hierarquia, lt2.permissao into #deletar from #t lt1              
  inner join #t lt2 on lt1.permissao = lt2.hierarquia and (lt1.hierarquia <> lt2.hierarquia and lt1.permissao=lt2.hierarquia) and lt2.profundidade >=0 order by lt1.hierarquia, lt1.permissao, lt2.profundidade              
               
delete t from #t t inner join #deletar d on t.hierarquia = d.hierarquia and t.permissao=d.permissao              
              
/** Criado o vol_lideresDiretos: acumulo de pontos da equipe dos lideres diretos *****************/              
select fhv.hierarquia, permissao              
 into #redeLideres              
    from fat_hierarquia_view fhv              
   inner join #rede_apuracao ra on fhv.permissao= ra.hierarquia and qualificacaoAtual >=20 and profundidade = 1              
 order by fhv.hierarquia               
/*                 
select rl.hierarquia, vol_grupo = isnull(sum(pontos),0)              
 into #vol_lideresDiretos               
 from #redeLideres rl              
  inner join #t fhv on rl.permissao=fhv.hierarquia and profundidade >0              
  inner join #rede_apuracao ra on ra.hierarquia=fhv.permissao               
 group by rl.hierarquia              
               
update a set vol_lideresDiretos = (select isnull(vol_grupo,0) from #vol_lideresDiretos b where b.hierarquia=a.hierarquia) from #rede_apuracao a               
 */              
              
              
select ra.hierarquia,               
    vol_lideresDiretos = sum(ra1.vol_grupo),              
    totNovosLideres = count(0)              
into #vol_lideresDiretos               
from #rede_apuracao ra              
inner join fat_hierarquia_view fhv on fhv.hierarquia = ra.hierarquia and fhv.profundidade = 1              
inner join #rede_apuracao ra1 on ra1.hierarquia = fhv.permissao and ra1.qualificacaoAtual >= 20 and ra1.data = ra.data              
where ra.data = @inicio              
group by ra.hierarquia              
              
 /***********************************************************************************************/              
              
/*Prata*/              
update ap set qualificacaoAtual = 22              
 from #rede_apuracao ap inner join fat_hierarquia fh on ap.hierarquia = fh.hierarquia inner join #vol_lideresDiretos ld on ap.hierarquia=ld.hierarquia              
 where isnull(ld.totNovosLideres,0) between 1 and 2 and ap.qualificacaoAtual >= 20 and ld.vol_lideresDiretos <= 14000              
              
--Atualizar a qualificacaoAtual conforme vol_lideresDiretos              
update ap set qualificacaoAtual = (select top 1 rq.qualificacao from rede_qualificacao rq                     
 where (isnull(ld.totNovosLideres,0) >= rq.master_min and rq.qualificacao > 10 and rq.qualificacao < 40) or (isnull(ld.vol_lideresDiretos,0) >= rq.minimo_pontos and rq.qualificacao > 10 and rq.qualificacao < 40) order by qualificacao desc)               
  
    
     
 from #rede_apuracao ap inner join #vol_lideresDiretos ld on ap.hierarquia=ld.hierarquia              
 where qualificacaoAtual>=20 and isnull(ld.totNovosLideres,0)>=1 and ap.hierarquia in (select hierarquia from #vol_lideresDiretos)              
/*              
update #rede_apuracao set qualificacaoAtual = 24 where hierarquia = 1226 and data = '2017-8-1'              
update #rede_apuracao set qualificacaoAtual = 20 where hierarquia = 1349 and data = '2017-8-1'              
*/               
--update ap set qualificacaoAtual = (select top 1 rq.qualificacao from rede_qualificacao rq                     
-- where (ap.totNovosLideres >= rq.master_min and rq.qualificacao > 10 and rq.qualificacao < 40) or (ap.faturamentoGrupo >= rq.minimo_pontos and rq.qualificacao > 10 and rq.qualificacao < 40) order by qualificacao desc)                    
-- from #rede_apuracao ap where qualificacaoAtual=20 or qualificacaoAtual>22 and ap.totNovosLideres>=1              
-- --inner join fat_hierarquia fh on fh.hierarquia = ap.hierarquia where fh.classificacao = 20                  
     
update a set qualificacaoAtual = 5 from #rede_apuracao a inner join fat_hierarquia fh on a.hierarquia = fh.hierarquia where fh.classificacao = 5                  
              
update a set qualificacaoAtual = 40 from #rede_apuracao a inner join fat_hierarquia fh on a.hierarquia = fh.hierarquia where fh.classificacao = 40                
              
update a set qualificacao = (              
--select isnull(max(qualificacaoAtual),10) from rede_apuracao b where a.hierarquia = b.hierarquia and ciclo < @ciclo group by hierarquia              
    isnull(ra2.qualificacaoAtual,10)              
    )               
from #rede_apuracao a --where a.hierarquia = b.hierarquia              
LEFT JOIN (              
 select ra1.hierarquia,qualificacaoAtual = max(qualificacaoAtual) from rede_apuracao ra1               
 left join #rede_lideres_hist rlh on rlh.hie = ra1.hierarquia              
 where   ra1.ciclo < @ciclo              
   and (              
    ra1.data > isnull(rlh.fim_vigencia,'1900-01-01')              
    )              
 group by ra1.hierarquia              
) ra2 on ra2.hierarquia = a.hierarquia              
              
update a set qualificacao = case when qualificacao >= qualificacaoAtual then qualificacao else qualificacaoAtual end from #rede_apuracao a              
              
update a              
set qualificacao = isnull(isnull(fdo.qualificacao,case when a.qualificacao >= a.qualificacaoAtual then a.qualificacao else a.qualificacaoAtual end),10)               
from #rede_apuracao a                                        
left join (              
 select fh.hierarquia,               
     qualificacao =max(condicao)              
 from rede_parametros rp               
 inner join fat_destinatario_ocorrencia fdo on fdo.ocorrencia in (331,332,333,334,335,336,337,338,339) and convert(date,fdo.data) between rp.inicio and rp.fim              
 inner join fat_ocorrencia fo on fo.ocorrencia = fdo.ocorrencia              
 inner join fat_hierarquia fh on fh.destinatario = fdo.destinatario              
 where rp.inicio = @inicio              
 group by fh.hierarquia              
) fdo on fdo.hierarquia = a.hierarquia               
--update #rede_apuracao set qualificacao = 20 where hierarquia = 1742              
                  
--select * from #rede_apuracao where hierarquia = 1742              
--VERIFICANDO SE CAIU DA QUALIFICA��O LIDER E FICOU REGISTRADO NA REDE_LIDERES              
--delete rl from #rede_apuracao ra              
--inner join rede_lideres rl on rl.hie = ra.hierarquia and ra.ciclo = ra.ciclo              
--where ISNULL(ra.qualificacao,10) < 20              
              
update a set qualificacaoAtual = 10 from #rede_apuracao a where qualificacaoAtual is null              
              
--pegar a maior qualificacao dos ultimos 3 meses caso seja lider              
/* RETIRADO EM 12/07/2017              
update a set qualificacao = 10              
from #rede_apuracao a where a.hierarquia in (1720,1726,1742)              
              
update a set classificacao = 10 from fat_hierarquia a where hierarquia in (select hierarquia from #rede_apuracao where qualificacao between 20 and 39)              
and a.hierarquia not in (1720,1726,1742)              
              
*/              
update a set classificacao = 20 from fat_hierarquia a where hierarquia in (select hierarquia from #rede_apuracao where qualificacao between 20 and 39)              
              
delete from #rede_apuracao where qualificacaoAtual = 5              
              
              
-------------------------------------------------------------------------------------------------------------------  
 ---------------------------------------------------- LIDERES QUE DESISTIRAM DE SER LIDERES -------------------------------------------------    
    
UPDATE ra    
SET qualificacao = 10    
FROM #rede_apuracao ra    
INNER JOIN view_consultor_v2 vc on vc.hierarquia = ra.hierarquia    
INNER JOIN fat_destinatario_ocorrencia fdo on fdo.destinatario = vc.destinatario and fdo.ocorrencia in (343)     
    
---------------------------------------------------------------------------------------------------------------------------------------------                
--------------------------------------------- Primeiro Lider ------------------------------------------------------                        
              
update f set master1 = isnull((select top 1 v.hierarquia from fat_hierarquia_view v                        
     inner join #rede_apuracao f1 on f1.hierarquia = v.hierarquia                        
where f.hierarquia = v.permissao and v.profundidade > 0 and f1.qualificacao >= 20 order by profundidade),0)                        
       from #rede_apuracao f                 
       where f.data = @inicio                         
                      
--update a set faturamentoGrupo = ISNULL((select sum(valores) from #pontoss                       
-- inner join fat_hierarquia_view fhv on #pontoss.hieComprador = fhv.permissao and fhv.hierarquia = a.hierarquia and fhv.profundidade > 0                      
-- inner join #rede_apuracao ra1 on fhv.permissao = ra1.hierarquia and ra1.master1 = fhv.hierarquia),0)                        
-- from #rede_apuracao a                        
-------------------------------------------------------------------------------------------------------------------              
-------------------------------------------------- B�nus Ades�o ---------------------------------------------------              
 select SUM(ValorTotalNF) as valorTotal, hieComprador              
  into #Adesao              
  from ped_PedidoTotal ppt              
  where ppt.stnfCodigo in (1, 2, 10, 37, 42, 43, 97, 98, 200)               
  and ppt.pedicodigo in (select distinct ppi.pedicodigo from ped_pedidoitem ppi               
        inner join sys_produto sp on sp.produto = ppi.prodCodigo              
        and sp.linha not in (11) where TipoOperacao IN (1/*,3*/)) /*retirado os kits upgrade de acordo com o chamado 10487*/              
 and ciclo = @ciclo              
  group by hieComprador              
              
  select a.hiecomprador, (a.valorTotal * .10) as valBonus, fh.nivel_superior as superior              
 into #BonAdesao from #Adesao a inner join fat_hierarquia fh on a.hieComprador = fh.hierarquia              
              
  select superior, sum(valBonus) as total into #BonusAdesao from #BonAdesao group by superior              
              
  update a set bonusAdesao = b.total from #rede_apuracao a inner join #BonusAdesao b on a.hierarquia = b.superior              
-------------------------------------------------------------------------------------------------------------------              
-----------------------------------------------------------------------------------------------------------------                  
              
              
-----------------------------------------------------------------------------------------------------------------                        
-------------------------------------------- B�nus Equipe ------------------------------------------------------               
delete rede_apuracaoBonusEquipe where ciclo = @ciclo              
              
insert into rede_apuracaoBonusEquipe(ciclo,hierarquia,permissao,profundidade,pedicodigo,pontos_calculados,porcentagem,bonus)              
select rav.ciclo,              
    rav.hierarquia,              
    rav.permissao,              
    rav.profundidade,              
    rav.pedicodigo,              
    rav.pontos_calculados,              
    porcentagem = case               
       when ra.qualificacaoatual > 10 and rav.profundidade = 1 then 0.1               
       when ra.qualificacaoatual > 10 and rav.profundidade > 1 then 0.05               
       when ra.qualificacaoatual = 10 then 0.05               
       else 0               
      end,              
    bonus=convert(numeric(18,2),isnull(rav.pontos_calculados,0) * case               
                   when ra.qualificacaoatual > 10 and rav.profundidade = 1 then 0.1               
                   when ra.qualificacaoatual > 10 and rav.profundidade > 1 then 0.05               
                   when ra.qualificacaoatual = 10 and rav.profundidade = 1 then 0.05             
                   else 0               
                  end)              
from #rede_apuracao ra              
inner join rede_apuracaoVolumeGrupo  rav on rav.hierarquia = ra.hierarquia and rav.ciclo = @ciclo              
              
/*Atualizar o b�nus equipe*/                
update a set bonusequipe = isnull((select bonus = sum(isnull(rabe.bonus,0)) from rede_apuracaoBonusEquipe rabe where rabe.hierarquia=a.hierarquia and rabe.ciclo = @ciclo group by rabe.hierarquia),0.00) from #rede_apuracao a               
              
              
-----------------------------------------------------------------------------------------------------------------                  
-------------------------------------------- B�nus Lideran�a ------------------------------------------------------                        
                        
--update a set BonusLideranca = a.faturamentoGrupo * (rq.bonus / 100) from #rede_apuracao a                        
--       inner join rede_qualificacao rq on a.qualificacao = rq.qualificacao where a.qualificacao >= 20                   
              
/*Caso mude de lideres diretos para compress�o de lideres */              
--select distinct fhv.*, ra.qualificacao as qualiSup, ra2.qualificacao, niveis=  DENSE_RANK () OVER (partition by permissao order by profundidade )              
-- into #fat_hierarquia_lideres              
-- from fat_hierarquia_view fhv       
--  inner join rede_apuracao ra on fhv.hierarquia = ra.hierarquia and ra.qualificacao >= 20 and ra.ciclo = @ciclo and profundidade > 0              
--  inner join rede_apuracao ra2 on fhv.permissao = ra2.hierarquia and ra2.qualificacao >= 20 and ra2.ciclo = @ciclo              
--  order by permissao, profundidade              
delete rede_apuracaoBonusLideranca where ciclo = @ciclo              
              
insert into rede_apuracaoBonusLideranca(hierarquia,permissao,qualificacaoAtual,pontos,porcentagem,bonus,ciclo)              
select rlb.hierarquia,               
    rlb.permissao,               
    ra.qualificacaoAtual,              
    pontos = ra.vol_grupo,              
    porcentagem = rq.bonus*0.01,              
    bonus = CASE WHEN  ra.qualificacaoAtual >= 20 THEN rq.bonus * 0.01 * isnull(ra.vol_grupo,0)              
     ELSE 0.00 END,              
    ciclo = @ciclo              
from rede_lideresBonusGeracao rlb              
left join #rede_apuracao ra on ra.hierarquia = rlb.permissao /*and ra.qualificacaoAtual >= 20*/              
inner join #rede_apuracao ra1 on ra1.hierarquia = rlb.hierarquia /*and ra1.qualificacaoAtual >= 22*/               
inner join rede_qualificacao rq on rq.qualificacao = ra1.qualificacaoAtual              
where /*ra.vol_grupo > 0               
   and */rlb.hierarquia <> rlb.permissao              
   and rlb.ciclo <= @ciclo              
                 
update ra               
set bonusLideranca = (select bonus = sum(isnull(rlb.bonus,0)) from rede_apuracaoBonusLideranca rlb where rlb.hierarquia= ra.hierarquia and rlb.ciclo = @ciclo group by rlb.hierarquia)               
from #rede_apuracao ra                 
              
-----------------------------------------------------------------------------------------------------------------                 
-------------------------------------------- B�nus Presidente ------------------------------------------------------                                        
--declare @sem int                
--select hierarquia, data, case when MONTH(data) <= 6 then 1 else 2 end as semestre into #Quali                                       
-- from rede_apuracao where faturamentoGrupo >= 7000                                      
                           
--select @sem = case when MONTH(@inicio) <= 6 then 1 else 2 end           
                                      
--select hierarquia, data into #quali1 from #quali where semestre = @sem                                    
                                      
--select hierarquia into #qualificados from #quali1 group by hierarquia having COUNT(hierarquia) >= 3                                      
                   
--if @sem = 1                
-- begin               
-- select q.hierarquia, SUM(ra.faturamentoGrupo) as vendaEquipe                                       
--  into #fim1 from #qualificados q inner join rede_apuracao ra on q.hierarquia = ra.hierarquia                
--where MONTH(ra.data) between 1 and 6 group by q.hierarquia                                      
                                       
-- select top 5 * into #fim1_1 from #fim1 order by vendaEquipe desc                            
                                       
-- alter table #fim1_1 add pos int identity                              
               
--select SUM(ValorTotalNF) as valorTotal                   
--  into #Presidente                          
--  from ped_PedidoTotal ppt                
-- inner join fat_COMERCIO fc on ppt.destinatario = fc.destinatario and fc.grupo >=4  /*Considerar apenas os consultores, ou seja, excluir os consumidores, distribuidores e Franquia-CD*/                                                                    
  
    
     
               
               
              
              
               
               
              
              
               
              
              
              
               
              
              
              
              
               
              
               
              
               
              
               
               
              
               
               
               
            
               
               
              
              
               
              
              
              
              
               
              
              
              
               
               
               
               
               
               
              
               
--  where ppt.stnfCodigo in (1, 2, 10, 37, 42, 43, 97, 98, 200) --and ppi.TipoOperacao <> 50                            
--  and ppt.pedicodigo not in (select pedicodigo from #kit_inicio) /*Excluir kit inicio (Gilson em 30/03/2017)*/                                            
--  and month(ppt.DataPedido) between 1 and 6 and YEAR(datapedido) = YEAR(@inicio)                                      
                                      
-- select vc.hierarquia, bonus = (p.valorTotal * .01) * case pos when 1 then .30 when 2 then .25 when 3 then .20 when 4 then .15 when 5 then .10 end                        
--  into #final1  from rede_apuracao a                                        
--   inner join #fim1_1 f on f.hierarquia = a.hierarquia                                      
--   inner join view_consultor vc on a.hierarquia = vc.hierarquia                                       
--   , #Presidente p                                       
                                         
-- update a set bonusPresidente = b.bonus from #rede_apuracao a, #final1 b where a.hierarquia = b.hierarquia                                      
               
-- end                                      
-- if @sem = 2                    
-- begin                                      
-- select q.hierarquia, SUM(ra.faturamentoGrupo) as vendaEquipe                                       
--  into #fim2 from #qualificados q inner join rede_apuracao ra on q.hierarquia = ra.hierarquia                                      
--  where MONTH(ra.data) between 7 and 12 group by q.hierarquia               
                 
--  select top 5 * into #fim2_1 from #fim2 order by vendaEquipe desc                                      
                                 
-- alter table #fim2_1 add pos int identity                                      
                
-- select SUM(ValorTotalNF) as valorTotal                                        
--  into #Presidente2                                          
--  from ped_PedidoTotal ppt                  
--    inner join fat_COMERCIO fc on ppt.destinatario = fc.destinatario and fc.grupo >=4  /*Considerar apenas os consultores, ou seja, excluir os consumidores, distribuidores e Franquia-CD*/                                 
--  where ppt.stnfCodigo in (1, 2, 10, 37, 42, 43, 97, 98, 200)                        
--  and ppt.pedicodigo not in (select pedicodigo from #kit_inicio) /*Excluir kit inicio (Gilson em 30/03/2017)*/                                            
--  and month(ppt.DataPedido) between 1 and 6 and YEAR(datapedido) = YEAR(@inicio)                        
                        
-- select vc.hierarquia, bonus = (p.valorTotal * .01) * case pos when 1 then .30 when 2 then .25 when 3 then .20 when 4 then .15 when 5 then .10 end                        
--  into #final2 from rede_apuracao a                                        
--   inner join #fim2_1 f on f.hierarquia = a.hierarquia                                     
--   inner join view_consultor vc on a.hierarquia = vc.hierarquia           
--   , #Presidente2 p                      
                              
-- update a set bonusPresidente = b.bonus from #rede_apuracao a, #final2 b where a.hierarquia = b.hierarquia and a.qualificacaoAtual >= 20                                    
                                       
-- end                                      
                 
---------------------------------------------------------------------------------------------              
 ---------------- Setar Ativo/Inativo mensal na rede_apuracao --------------------                 
               
select distinct               
 fh.destinatario,              
 hie = fh.hierarquia              
into #ativos               
from view_consultorAtivacao_t vca               
inner join fat_hierarquia fh on fh.destinatario = vca.destinatario              
where vca.ciclo = @ciclo              
              
update ra               
set ra.ativo = 1               
from #rede_apuracao ra              
where hierarquia in (select hierarquia from #ativos)              
                                              
-------------------------------------------- B�nus Gera��o ------------------------------------------------------                  
--criar tabela de Lideres para o B�nus Gera��o--------------------------------------------------------------------              
              
select fhv.*,               
    niveis = fhv.profundidade              
into #tab_lideres              
from fat_hierarquia_view fhv              
inner join #rede_apuracao ra on fhv.hierarquia = ra.hierarquia and ra.ciclo = @ciclo and ra.qualificacao >=20              
inner join #rede_apuracao ra2 on fhv.permissao = ra2.hierarquia and ra2.ciclo = @ciclo and ra2.qualificacao >=20              
where fhv.profundidade = 1              
order by permissao,profundidade               
              
--IDENTIFICANDO NOVOS LIDERES              
delete rede_lideresBonusGeracao where ciclo = @ciclo              
insert into rede_lideresBonusGeracao              
select tl.hierarquia,              
    tl.permissao,              
    ciclo = @ciclo,              
    obs = 'Sistema'              
from #tab_lideres tl               
left join rede_lideresBonusGeracao rlbg on tl.permissao = rlbg.permissao               
where niveis = 1               
   and rlbg.hierarquia is null              
              
delete rede_apuracaoBonusGeracao where ciclo = @ciclo              
--INSERINDO B�NUS REFERENTE AOS 2% DO VOLUME DA REDE DO LIDER GERADO              
insert into rede_apuracaoBonusGeracao(hierarquia,permissao,qualificacaoAtual,pontos,porcentagem,bonus,ciclo)              
select rlb.hierarquia,               
    rlb.permissao,               
    ra.qualificacaoAtual,              
    pontos = ra.vol_grupo,              
    porcentagem = 0.02,              
    bonus = CASE WHEN ra.ativo = 1 and ra.qualificacaoAtual >= 20 THEN 0.02 * isnull(ra.vol_grupo,0)              
     ELSE 0.00 END,              
    ciclo = @ciclo              
from rede_lideresBonusGeracao rlb              
inner join #rede_apuracao ra on ra.hierarquia = rlb.permissao               
inner join #rede_apuracao ra1 on ra1.hierarquia = rlb.hierarquia and ra1.qualificacao >= 20              
where /*ra.vol_grupo > 0 and*/ rlb.hierarquia <> rlb.permissao              
   and rlb.ciclo <= @ciclo              
              
--INSERINDO B�NUS REFERENTE AOS NOVOS LIDERES GERADOS              
insert into rede_apuracaoBonusGeracao(hierarquia,permissao,qualificacaoAtual,pontos,porcentagem,bonus,ciclo)              
select rlb.hierarquia,               
    rlb.permissao,               
    ra.qualificacaoAtual,              
    pontos = ra.vol_grupo,              
    porcentagem = 100.00,              
    bonus = 400.00,              
    ciclo = @ciclo              
from rede_lideresBonusGeracao rlb              
inner join #rede_apuracao ra on ra.hierarquia = rlb.permissao                
inner join #rede_apuracao ra1 on ra1.hierarquia = rlb.hierarquia and ra1.qualificacao >= 20              
where rlb.ciclo = @ciclo and rlb.hierarquia <> rlb.permissao              
              
update ra              
set    ra.bonusGeracao = (select sum(rab.bonus) from rede_apuracaoBonusGeracao rab where rab.hierarquia = ra.hierarquia and rab.ciclo = @ciclo)              
from #rede_apuracao ra              
              
------------------------------------------------------------------------------------------------------------------              
              
----------------------------------------------------B�NUS PROXIMIDADE---------------------------------------------              
              
delete a from rede_fechamento_proximidade a where ciclo = @ciclo              
              
--PEDIDOS PROXIMOS QUE J� FORAM PAGOS OU RETIRADOS              
insert into rede_fechamento_proximidade(ciclo,pedicodigo,rota,cep,valorTotalNf,hieProximo,cepProximo,porcentagem,bonus,processado)              
select ciclo = ppt.ciclo,         
    ppt.pedicodigo,              
    ppt.rota,              
       fd.cep,              
    ppt.ValorTotalNf,              
    hieProximo = NULL,              
    cepProximo = NULL,              
    porcentagem = 0.15,              
    bonus = convert(numeric(18,2),0.00),              
    processado = 0              
from dbo.ped_pedidototal ppt              
inner join dbo.fat_comercio fc on ppt.destinatario = fc.destinatario and fc.grupo = 1              
inner join dbo.fat_destinatario fd on fd.destinatario = fc.destinatario and REPLACE(fd.cep,'-','') like '%[0-9]%'              
inner join rede_parametros rp on rp.ciclo = ppt.ciclo              
where ppt.ciclo >= 35               
   and ppt.ciclo = @ciclo              
   and ppt.stnfcodigo in (1, 2, 10, 11, 12, 97, 98, 200)              
              
declare @consultorProximo table (              
 cepProximo varchar(8),              
 hieProximo int              
)              
              
declare @pedidoProximo int = 0, @cepPedido varchar(8)              
              
--ATUALIZA DADOS DA CONSULTA DE CEP  PROXIMO              
exec loja_calcula_desconto_sp              
              
while (select count(0) from rede_fechamento_proximidade where processado = 0) > 0              
begin              
 delete @consultorProximo              
 select @pedidoProximo = 0              
              
 --DEFINE QUAL PEDIDO PROXIMO SER� PROCESSADO              
 select top 1 @pedidoProximo = isnull(pedicodigo,0),              
     @cepPedido = cep               
 from rede_fechamento_proximidade               
 where processado = 0              
              
 if @pedidoProximo > 0              
 begin              
  --IDENTIFICA O CONSULTOR MAIS PROXIMO AO CEP DO DESTINATARIO DO PEDIDO              
  insert into @consultorProximo              
  exec buscar_cep_proximo_sp @cepPedido,1,@inicio              
              
  --ATUALIZA TABELA TEMPOR�RIA DE APURA��O              
  update a              
  set a.hieProximo = b.hieProximo,              
   a.cepProximo = b.cepProximo,              
   a.bonus = a.ValorTotalNf * a.porcentagem              
  from rede_fechamento_proximidade a              
  inner join (select top 1 *, cepPedido = @cepPedido from @consultorProximo) b on b.cepPedido = @cepPedido              
  where a.pedicodigo = @pedidoProximo and a.cep = @cepPedido              
 end              
              
 update a set processado = 1 from rede_fechamento_proximidade a where pedicodigo = @pedidoProximo and cep = @cepPedido              
              
end              
              
update ra              
set bonusProximidade = bp.bonusProximidade              
from #rede_apuracao ra              
inner join (select hieProximo,bonusProximidade = sum(isnull(bonus,0.00)) from rede_fechamento_proximidade where ciclo = @ciclo group by hieProximo) bp on ra.hierarquia = bp.hieProximo              
where ra.data =@inicio              
              
------------------------------------------------FIM B�NUS PROXIMIDADE---------------------------------------------              
              
----------------------------------------------INICIO SUPER B�NUS--------------------------------------------              
DECLARE @valorFaturamento numeric(18,2) = 0.00              
DECLARE @ciclosTable TABLE (              
 ciclo int,              
inicio datetime              
)              
    --�LTIMOS SEIS MESES              
insert into @ciclosTable              
select ciclo  = rp.ciclo,              
    inicio = rp.inicio               
from rede_parametros rp              
where rp.ciclo between @ciclo-5 and @ciclo              
              
--FATURAMENTO TOTAL              
 -- 1% DO VOLUME              
 -- PEDIDOS COM FATURAMENTO SEM CANCELAMENTO              
select               
   @valorFaturamento = sum(ppt.valorTotalNf) * 0.01              
from ped_pedidototal ppt              
inner join @ciclosTable ct on ct.ciclo = ppt.ciclo              
inner join fat_destinatario fd on fd.destinatario = ppt.destinatario and fd.nome not like '%teste%'              
where ppt.stnfCodigo not in (              
 20,21,27,28,99         
) and exists (             
 select 1 from ped_movimentostatus pms where pms.stnfcodigo = 1 and pms.pedicodigo = ppt.pedicodigo              
);              
              
with valorPorVolumeGrupo as (              
 select hierarquia,              
     valorTotalNf = sum(valorTotalNf)               
 from (              
   select                --FORMA DE APURA��O DO VOLUME DE GRUPO ATUAL              
    ra.hierarquia,              
    valorTotalNf = ra.valorTotalNf              
   from rede_apuracaoVolumeGrupo ra              
   inner join @ciclosTable ct on ct.ciclo = ra.ciclo              
   union all              
   select fhv.hierarquia, --FORMA DE APURA��O DO VOLUME DE GRUPO ANTIGO              
       valorTotalNf = ppt.valortotalNF              
   from fat_hierarquia_view fhv              
   inner join ped_pedidototal ppt on ppt.hieComprador = fhv.permissao and ppt.ciclo < 36              
   inner join @ciclosTable ct on ct.ciclo = ppt.ciclo              
   where fhv.profundidade = 1              
      and ppt.stnfCodigo not in (              
      20,21,27,28,99              
      ) and exists (              
      select 1 from ped_movimentostatus pms where pms.stnfcodigo = 1 and pms.pedicodigo = ppt.pedicodigo              
      )              
 ) as valorPorVolumeGrupo               
 group by hierarquia              
)              
              
select hierarquia,              
    valorTotalNf,              
    nivel = ROW_NUMBER() OVER(order by valorTotalNf desc,hierarquia)              
into #valorPorVolumeGrupo               
from valorPorVolumeGrupo               
group by hierarquia,valorTotalNf               
              
delete rede_apuracaoSuperBonus where ciclo = @ciclo              
              
insert into rede_apuracaoSuperBonus(hierarquia,ciclo,nivel,porcentagem,valorTotalNf,bonus,totalFaturamento)              
select              
  fh.hierarquia,              
  ciclo = @ciclo,              
  vg.nivel,              
  porcentagem = isnull(rpsb.porcentagem,0.00),              
  vg.valorTotalNf,              
  bonus =  @valorFaturamento * ( isnull(rpsb.porcentagem,0.00) / 100.00 ),              
  totalFaturamento = @valorFaturamento              
from fat_hierarquia fh              
inner join #rede_apuracao ra on ra.hierarquia = fh.hierarquia and ra.data = @inicio              
left join #valorPorVolumeGrupo vg on vg.hierarquia = fh.hierarquia              
left join rede_parametrosSuperBonus rpsb on rpsb.nivel = vg.nivel              
where exists (          
 select 1 from @ciclosTable ct              
 inner join rede_apuracao ra on ra.hierarquia = fh.hierarquia and ra.data = ct.inicio and ra.ativo = 1 and ra.qualificacao >= 22              
 left join #rede_apuracao ra1 on ra1.hierarquia = ra.hierarquia and ra1.data = @inicio              
 where ra.hierarquia = fh.hierarquia              
    and ra.ciclo < @ciclo              
 group by ra.hierarquia,              
       ra1.qualificacao              
 having ( count(0) >= 3 ) or (count(0) = 2 and isnull(ra1.qualificacao,0) >= 22 )              
)              
              
update ra               
set superBonus = isnull((              
 select sum(bonus) from rede_apuracaoSuperBonus rasb where rasb.hierarquia = ra.hierarquia and rasb.ciclo = @ciclo              
),0.00)              
from #rede_apuracao ra               
              
------------------------------------------------FIM SUPER B�NUS---------------------------------------------              
              
              
------------------------------------------------ Atualizar Qualifica��o de descontos na fat_comercio (faixa_preco) ---------------------------------------------              
              
--Cria tempor�ria com todas as hierarquia, incluindo o ciclo              
select hierarquia, DataCadastro, ciclo into #fhCiclo from fat_hierarquia fh               
 inner join rede_parametros rp on fh.DataCadastro between inicio and fim+1 and fh.DataCadastro >= '2017-03-01'              
              
--Cria tempor�ria com os cadastros do mes atual (para inserir faixa_preco = 4)              
select hierarquia, fh.destinatario, dataCadastro               
 into #cadastrosMes               
 from fat_hierarquia fh              
   inner join fat_comercio fc on fh.destinatario = fc.destinatario               
 where fh.DataCadastro between @inicio and @fim+1 and fh.DataCadastro >= '2017-03-01' and fc.grupo >= 4              
              
--rede_apuracao dos 3 ciclos anteriores ao atual              
select ra.hierarquia, ra.data, pontos, dataCadastro into #raDescontos from rede_apuracao ra              
 inner join #fhCiclo a on ra.hierarquia = a.hierarquia and ra.ciclo>= a.ciclo              
 where ra.ciclo between @ciclo-3 and @ciclo-1              
              
--Tempor�ria j� com o denominador (para fazer a m�dia de pontos)              
select integracao, fh.destinatario, pontosMedia = sum(pontos)/ count(data), denominador = count(data), faixa_preco = '', ra.dataCadastro              
 into #hieDescontos from #raDescontos ra              
    inner join fat_hierarquia fh on ra.hierarquia = fh.hierarquia              
    inner join fat_comercio fc on fh.destinatario = fc.destinatario              
   group by integracao, fh.destinatario, ra.dataCadastro order by integracao              
              
--Inseri a faixa_preco em #hieDescontos              
update a set faixa_preco = case when pontosMedia <= 150 then 5              
        when pontosMedia > 150 and pontosMedia < 300 then 6              
        when pontosMedia > 300 then 7 end              
      from #hieDescontos a              
              
--Atualiza de acordo com a m�dia progressiva de 3 ciclos               
update fc set faixa_preco = (select faixa_preco from #hieDescontos a where fc.destinatario = a.destinatario), grupo = (select faixa_preco from #hieDescontos a where fc.destinatario = a.destinatario)              
      from fat_comercio fc where  fc.destinatario in (select destinatario from #hieDescontos)         
   and exists (select ppi.* from ped_pedidototal as ppt inner join ped_pedidoitem as ppi on ppi.pediCodigo = ppt.pediCodigo where ppi.TipoOperacao = 0 and ppt.destinatario = fc.destinatario)              
              
--Atualiza com faixa_preco = 4 para os novos cadastros              
update a set faixa_preco = 4, grupo = 4 from fat_comercio a where a.destinatario in (select destinatario from #cadastrosMes)              
              
              
              
   ----------------------- Fim -----------------------               
              
              
/************************************************************* Qualificacao para descontos por ocorrencia (fat_comercio) = Grupo de descontos *****************************************************************/              
              
--Consumidor              
update fc set faixa_preco = 1, grupo = 1 from fat_comercio fc inner join fat_destinatario_ocorrencia fdo on fc.destinatario = fdo.destinatario inner join fat_ocorrencia fo on fdo.ocorrencia = fo.ocorrencia and fo.ocorrencia = 19              
              
--Consultor              
update fc set faixa_preco = 4, grupo = 4 from fat_comercio fc inner join fat_destinatario_ocorrencia fdo on fc.destinatario = fdo.destinatario inner join fat_ocorrencia fo on fdo.ocorrencia = fo.ocorrencia and fo.ocorrencia = 18              
              
--Consultor Bronze              
update fc set faixa_preco = 5, grupo = 5 from fat_comercio fc inner join fat_destinatario_ocorrencia fdo on fc.destinatario = fdo.destinatario inner join fat_ocorrencia fo on fdo.ocorrencia = fo.ocorrencia and fo.ocorrencia = 17              
              
--Consultor Prata              
update fc set faixa_preco = 6, grupo = 6 from fat_comercio fc inner join fat_destinatario_ocorrencia fdo on fc.destinatario = fdo.destinatario inner join fat_ocorrencia fo on fdo.ocorrencia = fo.ocorrencia and fo.ocorrencia = 16              
              
--Consultor Ouro              
update fc set faixa_preco = 7, grupo = 7 from fat_comercio fc inner join fat_destinatario_ocorrencia fdo on fc.destinatario = fdo.destinatario inner join fat_ocorrencia fo on fdo.ocorrencia = fo.ocorrencia and fo.ocorrencia = 14              
              
              
              
/************************************************************* Acrescentando Sinalizador de Cr�dito de compras/Pagamento de B�nus *****************************************************************/              
              
--select hierarquia, qualificacaoAtual=isnull(max(qualificacaoAtual),0) into #listaLideres from rede_apuracao where ciclo between @ciclo-2 and ciclo group by hierarquia              
select hierarquia, qualificacaoAtual=isnull(qualificacaoAtual,0) into #listaLideres from rede_apuracao where ciclo = @ciclo               
              
select *, qualificacaoPagamentoBonus = case when qualificacaoAtual >= 20 then 'Lider' else 'Consultor' end into #listaQualificacaoBonus from #listaLideres             
              
update a set qualificacaoPagamentoBonus = (select qualificacaoPagamentoBonus from #listaQualificacaoBonus b where a.hierarquia=b.hierarquia) from #rede_apuracao a              
              
               
----------------------- Fim -----------------------               
              
              
  delete from rede_apuracao where data = @inicio                        
                                                    
  insert into rede_apuracao              
  select * from #rede_apuracao              
              
  -- envia os emails para os lideres - chamado 10050               
  --exec email_solicitacao_lider_sp @ciclo              
              
              
/*Considera��es              
              
Dia 07/06/2017: foi acrescentado estrutura que atualiza a qualifica��o para descontos (faixa_preco da fat_comercio) - Gilson              
Dia 29/06/2017: acrescentado um sinalizador para pagamento de bonus ou disponibilidade de cr�dito em compras (Consultor/Lider) - Gilson               
              
*/ 