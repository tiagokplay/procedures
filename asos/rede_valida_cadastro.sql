alter PROC Rede_valida_cadastro_novo_sp 
@nome           VARCHAR(80), 
@nome_abreviado VARCHAR(80), 
@cpf            VARCHAR(14), 
@cep            VARCHAR(9), 
@endereco       VARCHAR(60), 
@numero         VARCHAR(15), 
@bairro         VARCHAR(60), 
@cidade         VARCHAR(40), 
@uf             VARCHAR(2), 
@logradouro     INT=0, 
@pais           VARCHAR(100) = 'Brasil' 
, 
@dddcel         VARCHAR(4) = '(11)', 
@fonecel        VARCHAR(10) = '11111-1111', 
@patrocinador   VARCHAR(50) 
AS 
    SET nocount ON 

    DECLARE @cod      INT = 0, 
            @msg      VARCHAR(200) ='', 
            @id       VARCHAR(40), 
            @fone1    VARCHAR(20), 
            @cont_cpf INT = 0 

    SELECT @cpf = Replace(Replace(@cpf, '.', ''), '-', '') 

    SELECT @cont_cpf = Count(destinatario) 
    FROM   fat_destinatario 
    WHERE  cgc_cpf = @cpf 

    IF ( Len(Isnull(@patrocinador, '')) = 0 ) 
      BEGIN 
          SELECT @cod = 99, 
                 @msg = 'Patrocinador n�o encontrado', 
                 @id = '#patrocinador' 

          GOTO fim 
      END 

    /* Revendedor n�o pode patrocinar, s� validando aqui pra tentar achar um possivel erro (kleber l. 26/05/2017) */
    IF ( Len(Isnull(@patrocinador, '')) > 0 ) 
      BEGIN 
          IF (SELECT Count(0) 
              FROM   fat_comercio 
              WHERE  grupo = 6 
                     AND integracao = @patrocinador) > 0 
            BEGIN 
                SELECT @cod = 99, 
                       @msg = 'Usu�rio inv�lido para ser um patrocinador!', 
                       @id = '#patrocinador' 

                GOTO fim 
            END 
      END 

    IF ( @cont_cpf > 0 ) 
      BEGIN 
          SELECT @cod = 11, 
                 @msg = 'CPF j� cadastrado, utilize um CPF v�lido', 
                 @id = '#cpf' 
      END 

    IF( @nome NOT LIKE '% %' ) 
      BEGIN 
          SELECT @cod = 31, 
                 @msg = 'Digite seu nome completo', 
                 @id = '#nome' 
      END 

    IF( @nome LIKE '%  %' ) 
      BEGIN 
          SELECT @cod = 32, 
                 @msg = 'Digite seu nome corretamente, remova espa�os duplos', 
                 @id = '#nome' 
      END 

    IF ( Len(Isnull(@fone1, '')) > 1 
         AND Len(Isnull(@fone1, '')) < 8 ) 
      BEGIN 
          SELECT @cod = 24, 
                 @msg = 'Numero de telefone invalido', 
                 @id = '#telefone' 

          GOTO fim 
      END 

    IF( Lower(@nome_abreviado) != @nome_abreviado COLLATE latin1_general_cs_ai ) 
      BEGIN 
          SELECT @cod = 30, 
                 @msg = 'Seu minisite n�o poder� possuir letras mai�sculas', 
                 @id = '#minisite' 

          GOTO fim 
      END 

    IF(SELECT Charindex(' ', @nome_abreviado)) > 0 
      BEGIN 
          SELECT @cod = 29, 
                 @msg = 'Seu minisite n�o poder� possuir espa�os em branco', 
                 @id = '#minisite' 

          GOTO fim 
      END 

    IF(SELECT Charindex('contem', @nome_abreviado)) > 0 
      BEGIN 
          SELECT @cod = 28, 
                 @msg = 'Seu minisite n�o poder� possuir a palavra CONTEM', 
                 @id = '#minisite' 

          GOTO fim 
      END 

    IF( @nome_abreviado LIKE '%[^a-zA-Z0-9]%' ) 
      BEGIN 
          SELECT @cod = 28, 
                 @msg = 'Seu minisite n�o poder� possuir caracteres especiais' 
                 , 
                 @id = '#minisite' 

          GOTO fim 
      END 

    IF Len(Isnull(Rtrim(Ltrim(@nome)), '')) = 0 
      BEGIN 
          SELECT @cod = 1, 
                 @msg = 'Preencha o campo nome � obrigatorio', 
                 @id = '#nome' 

          GOTO fim 
      END 

    IF Len(Rtrim(Ltrim(Isnull(@nome_abreviado, '')))) = 0 
      BEGIN 
          SELECT @cod = 2, 
                 @msg = 'Preencha o campo nome reconhecimento � obrigatorio', 
 @id = '#nome_reconhecimento' 

          GOTO fim 
      END 

    IF Len(Isnull(@cep, '')) = 0 
      BEGIN 
          SELECT @cod = 6, 
                 @msg = 'Preencha o campo CEP � obrigatorio', 
                 @id = '#cep' 

          GOTO fim 
      END 

    IF Len(Isnull(@endereco, '')) = 0 
      BEGIN 
          SELECT @cod = 7, 
                 @msg = 'Preencha o campo logradouro � obrigatorio', 
                 @id = '#logradouro' 

          GOTO fim 
      END 

    IF Len(Isnull(@numero, '')) = 0 
      BEGIN 
          SELECT @cod = 8, 
                 @msg = 'Preencha o campo numero � obrigatorio', 
                 @id = '#numero' 

          GOTO fim 
      END 

    IF Len(Isnull(@bairro, '')) = 0 
      BEGIN 
          SELECT @cod = 9, 
                 @msg = 'Preencha o campo bairro � obrigatorio', 
                 @id = '#bairro' 

          GOTO fim 
      END 

    IF Len(Isnull(@uf, '')) = 0 
      BEGIN 
          SELECT @cod = 10, 
                 @msg = 'Preencha o campo estado � obrigatorio', 
                 @id = '#uf' 

          GOTO fim 
      END 

    IF Len(@logradouro) = 0 
       AND @logradouro = 0 
      BEGIN 
          SELECT @cod = 22, 
                 @msg = 'Preencha o campo logradouro � obrigatorio', 
                 @id = '#logradouro' 

          GOTO fim 
      END 

    IF ( Len(Isnull(@dddcel, '')) = 0 
          OR @dddcel = '00' ) 
      BEGIN 
          SELECT @cod = 26, 
                 @msg = 'Preencha o campo ddd do celular � obrigatorio', 
                 @id = '#celular_ddd' 

          GOTO fim 
      END 

    IF Len(Isnull(@fonecel, '')) = 0 
      BEGIN 
          SELECT @cod = 27, 
                 @msg = 'Preencha o campo celular � obrigatorio', 
                 @id = '#celular' 

          GOTO fim 
      END 

    --caso tenha passado por todas as valida��es verifica se ainda continua com @cod=0 (sucesso) 
    IF( @cod = 0 ) 
      BEGIN 
          SELECT @cod = 0, 
                 @msg = 'Passou por todas as valida��es', 
                 @id = '' 
      END 

    FIM: 

    SELECT cod = @cod, 
           msg = @msg, 
           id = @id 

    SET nocount OFF 


